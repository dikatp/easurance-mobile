import 'package:easurance_mobile/models/Auth.dart';
import 'package:easurance_mobile/widgets/CarWishlistTab.dart';
import 'package:easurance_mobile/widgets/PropertyWishlistTab.dart';
import 'package:easurance_mobile/widgets/TravelWishlistTab.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;

class WishlistPage extends StatefulWidget {
  @override
  _WishlistPageState createState() => _WishlistPageState();
}

class _WishlistPageState extends State<WishlistPage> {
  Auth auth = new Auth();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.directions_car)),
              Tab(icon: Icon(Icons.home)),
              Tab(icon: Icon(Icons.airplanemode_active)),
            ],
          ),
          backgroundColor: utils.primaryColor,
          title: Text(
            'Wishlist',
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: utils.screenAwareSize(18.0, context)),
          ),
        ),
        body: TabBarView(
          children: [
            CarWishlistTab(),
            PropertyWishlistTab(),
            TravelWishlistTab(),
          ],
        ),
      ),
    );
  }
}
