import 'package:easurance_mobile/models/InsuranceBenefit.dart';
import 'package:easurance_mobile/models/PropertyCity.dart';
import 'package:easurance_mobile/models/PropertyInsurancePremi.dart';
import 'package:easurance_mobile/models/PropertyProvince.dart';
import 'package:easurance_mobile/widgets/SmartSystemDialog.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/models/Auth.dart';
import 'package:easurance_mobile/pages/LoginPage.dart';
import 'package:easurance_mobile/widgets/BenefitDialog.dart';
import 'package:easurance_mobile/widgets/SortDialog.dart';
import 'package:easurance_mobile/utils.dart' as utils;
import 'package:intl/intl.dart' as intl;
import 'package:flutter_speed_dial/flutter_speed_dial.dart' as fab;

class PropertyInsurancePremiPage extends StatefulWidget {
  final PropertyProvince selectedProvince;
  final PropertyCity selectedCity;
  final String address;
  final int value;
  final String selectedFlexas;

  const PropertyInsurancePremiPage(
      {Key key,
      this.selectedProvince,
      this.selectedCity,
      this.address,
      this.value,
      this.selectedFlexas})
      : super(key: key);

  @override
  _PropertyInsurancePremiPageState createState() =>
      _PropertyInsurancePremiPageState();
}

class _PropertyInsurancePremiPageState
    extends State<PropertyInsurancePremiPage> {
  final Auth auth = new Auth();
  final idrCurrency = new intl.NumberFormat("#,##0", "id_ID");
  Future<List<PropertyInsurancePremi>> premis;
  List<String> additional = [];
  String sortBy, sortByIndicator;
  bool asc;
  List<PropertyInsurancePremi> currPremi;

  @override
  void initState() {
    sortBy = "name";
    sortByIndicator = "name_asc";
    asc = true;
    super.initState();
    _fetchPremi();
  }

  String _priceNameNormalize(String value) {
    String nameNormalize;
    List<String> splitList = value.split('_');
    if (splitList.length > 1) {
      if (splitList[1].contains('comp')) {
        nameNormalize = splitList[0].toUpperCase();
      } else {
        for (var i = 0; i < splitList.length; i++) {
          splitList[i] =
              splitList[i][0].toUpperCase() + splitList[i].substring(1);
        }
        nameNormalize = splitList.join(' ');
      }
    } else {
      nameNormalize = splitList[0].toUpperCase();
    }
    return nameNormalize;
  }

  Widget checkbox(String title, bool boolValue) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: boolValue,
          onChanged: (bool value) {
            setState(() {
              switch (title) {
                case "TSFWD":
                  if (value)
                    additional.add('tsfwd');
                  else
                    additional.remove('tsfwd');
                  break;
                case "RSMDCC":
                  if (value)
                    additional.add('rsmdcc');
                  else
                    additional.remove('rsmdcc');
                  break;
                case "EQVET":
                  if (value)
                    additional.add('eqvet');
                  else
                    additional.remove('eqvet');
                  break;
                case "Other":
                  if (value)
                    additional.add('other');
                  else
                    additional.remove('other');
                  break;
              }
              _fetchPremi();
            });
          },
        ),
        Text(
          title,
          style: TextStyle(fontSize: utils.screenAwareSize(12, context)),
        )
      ],
    );
  }

  void _fetchPremi() {
    setState(() {
      premis = PropertyInsurancePremi.loadList(
        widget.value,
        widget.selectedFlexas,
        widget.selectedCity.id.toString(),
        additional,
      );
    });
  }

  Future _openBenefitDialog(
      List<InsuranceBenefits> benefits, String name) async {
    await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return BenefitDialog(insuranceName: name, benefits: benefits);
        });
  }

  void _openSortDialog() async {
    final String result =
        await Navigator.of(context).push(MaterialPageRoute<String>(
            builder: (BuildContext context) {
              return SortDialog(
                sortByIndicator: sortByIndicator,
              );
            },
            fullscreenDialog: true));
    if (result != null) {
      List<String> splitResult = result.split('_');
      setState(() {
        sortByIndicator = result;
        sortBy = splitResult[0];
        splitResult[1] == 'asc' ? asc = true : asc = false;
      });
      setState(() {
        currPremi = PropertyInsurancePremi.sort(currPremi, sortBy, asc);
      });
    }
  }

  void _openSmartSystemDialog() async {
    final List<InsuranceBenefits> result = await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return SmartSystemDialog(benefitType: 'property');
        });
    if (result != null) {
      setState(() {
        premis = PropertyInsurancePremi.smartSystem(
            widget.value,
            widget.selectedFlexas,
            widget.selectedCity.id.toString(),
            additional,
            result);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: utils.primaryColor,
        title: Text(
          'Property Insurance Premi',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      floatingActionButton: fab.SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22),
        curve: Curves.bounceIn,
        elevation: 10.0,
        shape: CircleBorder(),
        children: [
          fab.SpeedDialChild(
              backgroundColor: Colors.grey,
              child: Icon(Icons.sort),
              label: 'Sort',
              labelStyle:
                  TextStyle(fontSize: utils.screenAwareSize(12, context)),
              onTap: () {
                _openSortDialog();
              }),
          fab.SpeedDialChild(
              backgroundColor: Colors.grey,
              child: Icon(Icons.search),
              label: 'Smart Search',
              labelStyle:
                  TextStyle(fontSize: utils.screenAwareSize(12, context)),
              onTap: () {
                _openSmartSystemDialog();
              })
        ],
      ),
      body: Column(
        children: <Widget>[
          ExpansionTile(
            title: Text(
              'Choose Additional Coverage',
              style: TextStyle(fontSize: utils.screenAwareSize(14, context)),
            ),
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      checkbox("TSFWD", additional.contains('tsfwd')),
                      checkbox("RSMDCC", additional.contains('rsmdcc')),
                      checkbox("EQVET", additional.contains('eqvet')),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      checkbox("Other", additional.contains('other')),
                    ],
                  ),
                ],
              )
            ],
          ),
          FutureBuilder(
              future: premis,
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.waiting:
                    return Center(
                        child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(utils.primaryColor),
                    ));
                  case ConnectionState.active:
                  case ConnectionState.done:
                    if (snapshot.hasError)
                      return Text("There was an error: ${snapshot.error}");
                    currPremi = snapshot.data;
                    return Expanded(
                        child: ListView.builder(
                      padding: EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 15.0),
                      itemCount: currPremi.length,
                      itemBuilder: (BuildContext context, int index) {
                        PropertyInsurancePremi premi = currPremi[index];
                        return Card(
                            child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  premi.matchValue > 0
                                      ? Text(
                                          premi.matchValue.toStringAsFixed(premi
                                                          .matchValue
                                                          .truncateToDouble() ==
                                                      premi.matchValue
                                                  ? 0
                                                  : 2) +
                                              '%',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        )
                                      : Text(''),
                                  Container(
                                    height: utils.screenAwareSize(50, context),
                                    width: utils.screenAwareSize(100, context),
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                      fit: BoxFit.fitWidth,
                                      image: NetworkImage(
                                        utils.url + premi.insuranceLogoPath,
                                      ),
                                    )),
                                  ),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    premi.insuranceName + ' Insurance',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        decoration: TextDecoration.underline),
                                  ),
                                  Divider(),
                                  Text(
                                    'Price Detail: ',
                                    style: TextStyle(
                                        fontSize:
                                            utils.screenAwareSize(13, context)),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: premi.prices
                                        .map(
                                          (p) => Text(
                                                _priceNameNormalize(p.name) +
                                                    ': Rp. ${idrCurrency.format(p.value)}',
                                                style: TextStyle(
                                                    fontSize:
                                                        utils.screenAwareSize(
                                                            12, context)),
                                              ),
                                        )
                                        .toList(),
                                  ),
                                  Divider(),
                                  Text(
                                      'Total Price: Rp. ${idrCurrency.format(premi.totalPrice)}',
                                      style: TextStyle(
                                          fontSize: utils.screenAwareSize(
                                              13, context))),
                                  Divider(),
                                  Row(children: <Widget>[
                                    GestureDetector(
                                        onTap: () {
                                          _openBenefitDialog(
                                              premi.insuranceBenefits,
                                              premi.insuranceName);
                                        },
                                        child: Container(
                                            height: utils.screenAwareSize(
                                                35, context),
                                            width: utils.screenAwareSize(
                                                65, context),
                                            child: Card(
                                              color: utils.primaryColor,
                                              child: Center(
                                                child: Text(
                                                  'Benefits',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:
                                                          utils.screenAwareSize(
                                                              10, context)),
                                                ),
                                              ),
                                            ))),
                                    GestureDetector(
                                        onTap: () async {
                                          if (await auth.check()) {
                                            bool success =
                                                await PropertyInsurancePremi
                                                    .saveToWishlist(
                                                        premi,
                                                        widget.selectedProvince
                                                            .name,
                                                        widget
                                                            .selectedCity.name,
                                                        widget.address,
                                                        widget.value,
                                                        auth.token);
                                            if (success) {
                                              Scaffold.of(context)
                                                  .showSnackBar(SnackBar(
                                                content: Text(
                                                    'Item added to wishlist'),
                                                backgroundColor: Colors.green,
                                              ));
                                            } else
                                              Scaffold.of(context)
                                                  .showSnackBar(SnackBar(
                                                content: Text('Error'),
                                                backgroundColor: Colors.red,
                                              ));
                                          } else
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        LoginPage()));
                                        },
                                        child: Container(
                                            height: utils.screenAwareSize(
                                                35, context),
                                            width: utils.screenAwareSize(
                                                65, context),
                                            child: Card(
                                              color: utils.primaryColor,
                                              child: Center(
                                                child: Text(
                                                  'Add to Wishlist',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:
                                                          utils.screenAwareSize(
                                                              10, context)),
                                                ),
                                              ),
                                            ))),
                                  ]),
                                ],
                              )
                            ],
                          ),
                        ));
                      },
                    ));
                }
              })
        ],
      ),
    );
  }
}
