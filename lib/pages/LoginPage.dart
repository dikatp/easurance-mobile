import 'package:easurance_mobile/pages/RegisterPage.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;
import '../models/Auth.dart';

class LoginPage extends StatefulWidget {
  final MaterialPageRoute intendedRoute;

  LoginPage({Key key, this.intendedRoute}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final Auth auth = new Auth(); //get auth class for auth function
  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  Future attemptLogin() async {
    bool success =
        await auth.login(_userNameController.text, _passwordController.text);
    if (success) {
      if (widget.intendedRoute != null) {
        Navigator.pushReplacement(
          context,
          widget.intendedRoute,
        );
      } else
        Navigator.pop(context, true);
    } else
      showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              'Data Invalid!',
              style: TextStyle(color: Colors.red),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('Your email or password is wrong.'),
                  Text('If you don\'t have an account, please sign up first!'),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
  }

  _registerAndDisplayResponse(BuildContext context) async {
    final result = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => RegisterPage()));

    if (result != null) {
      setState(() {
        _userNameController.text = result;
      });
      Scaffold.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(
          content: Text(
            "Account Created",
          ),
          backgroundColor: utils.primaryColor,
        ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: utils.primaryColor,
        title: Text(
          'Login',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(
          horizontal: 60,
        ),
        child: Column(
          children: <Widget>[
            SizedBox(
              width: utils.screenAwareSize(140, context),
              height: utils.screenAwareSize(140, context),
              child: Image.asset(
                "images/logo/logo.png",
                color: utils.primaryColor,
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(
              height: utils.screenAwareSize(50, context),
            ),
            TextField(
              controller: _userNameController,
              obscureText: false,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.email),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: "Email",
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: utils.primaryColor))),
              keyboardType: TextInputType.emailAddress,
            ),
            SizedBox(
              height: utils.screenAwareSize(20, context),
            ),
            TextField(
              controller: _passwordController,
              obscureText: true,
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.lock),
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                hintText: "Password",
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Material(
              elevation: 3.0,
              borderRadius: BorderRadius.circular(20.0),
              color: utils.primaryColor,
              child: MaterialButton(
                minWidth: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                onPressed: () {
                  attemptLogin();
                },
                child: Text(
                  "Login",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Don\'t have an acount? '),
                Builder(
                  builder: (context) => FlatButton(
                      child: Text(
                        'Sign Up',
                        style: TextStyle(color: utils.primaryColor),
                      ),
                      onPressed: () {
                        _registerAndDisplayResponse(context);
                      }),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
