import 'package:easurance_mobile/models/Faq.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;

class FaqPage extends StatefulWidget {
  @override
  _FaqPageState createState() => _FaqPageState();
}

class _FaqPageState extends State<FaqPage> {
  Future<List<Faq>> faqs;

  @override
  void initState() {
    super.initState();

    faqs = Faq.loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              var _faqs = Faq.loadList();

              setState(() {
                faqs = _faqs;
              });
            },
          )
        ],
        backgroundColor: utils.primaryColor,
        title: Text(
          'FAQ',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      body: FutureBuilder(
        future: faqs,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Center(
                  child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(utils.primaryColor),
              ));
            case ConnectionState.done:
              if (snapshot.hasError)
                return Text("There was an error: ${snapshot.error}");
              var faqs = snapshot.data;
              return ListView.builder(
                itemCount: faqs.length,
                itemBuilder: (BuildContext context, int index) {
                  Faq faq = faqs[index];
                  return ExpansionTile(
                    title: Text(
                      faq.question,
                      style: TextStyle(
                          fontSize: utils.screenAwareSize(16, context),
                          fontWeight: FontWeight.bold),
                    ),
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 8.0),
                        child: Text(
                          faq.answer,
                          textAlign: TextAlign.justify,
                        ),
                      )
                    ],
                  );
                },
              );
          }
        },
      ),
    );
  }
}
