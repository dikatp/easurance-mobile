import 'package:easurance_mobile/models/Auth.dart';
import 'package:easurance_mobile/models/CarInsurancePremi.dart';
import 'package:easurance_mobile/models/CarSeries.dart';
import 'package:easurance_mobile/models/InsuranceBenefit.dart';
import 'package:easurance_mobile/pages/LoginPage.dart';
import 'package:easurance_mobile/widgets/BenefitDialog.dart';
import 'package:easurance_mobile/widgets/SmartSystemDialog.dart';
import 'package:easurance_mobile/widgets/SortDialog.dart';
import 'package:easurance_mobile/widgets/WorkshopDialog.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;
import 'package:intl/intl.dart' as intl;
import 'package:flutter_speed_dial/flutter_speed_dial.dart' as fab;

class CarInsurancePremiPage extends StatefulWidget {
  final String carName;
  final CarSeries selectedSeries;
  final String selectedPlate;
  final String selectedYear;

  const CarInsurancePremiPage({
    Key key,
    this.selectedSeries,
    this.selectedPlate,
    this.selectedYear,
    this.carName,
  }) : super(key: key);

  @override
  _CarInsurancePremiPageState createState() => _CarInsurancePremiPageState();
}

class _CarInsurancePremiPageState extends State<CarInsurancePremiPage> {
  final Auth auth = new Auth();
  final idrCurrency = new intl.NumberFormat("#,##0", "id_ID");
  Future<List<CarInsurancePremi>> premis;
  String _radioValue;
  String selectedType;
  int paDriverVal, paPassengerVal, paPassengerSeats, tplVal;
  List<String> additional = [];
  String sortBy, sortByIndicator;
  bool asc;
  List<CarInsurancePremi> currPremi;
  List<InsuranceBenefits> selectedBenefits;

  @override
  void initState() {
    super.initState();
    setState(() {
      _radioValue = 'comp';
      selectedType = 'comp';
      tplVal = 1000000;
      paDriverVal = 5000000;
      paPassengerVal = 5000000;
      paPassengerSeats = 1;
      sortBy = "name";
      sortByIndicator = "name_asc";
      asc = true;
    });
    _fetchPremi();
  }

  @override
  void dispose() {
    super.dispose();
  }

  String _priceNameNormalize(String value) {
    String nameNormalize;
    List<String> splitList = value.split('_');
    if (splitList.length > 1) {
      if (splitList[1].contains('comp')) {
        nameNormalize = splitList[0].toUpperCase();
      } else {
        for (var i = 0; i < splitList.length; i++) {
          splitList[i] =
              splitList[i][0].toUpperCase() + splitList[i].substring(1);
        }
        nameNormalize = splitList.join(' ');
      }
    } else {
      nameNormalize = splitList[0].toUpperCase();
    }
    return nameNormalize;
  }

  void _fetchPremi() {
    setState(() {
      premis = CarInsurancePremi.loadList(
          widget.selectedSeries.id.toString(),
          widget.selectedPlate,
          selectedType,
          widget.selectedYear,
          additional,
          tplVal,
          paDriverVal,
          paPassengerVal,
          paPassengerSeats);
    });
  }

  void _changeInsuranceType(String value) {
    setState(() {
      _radioValue = value;
      selectedType = _radioValue;
      _fetchPremi();
    });
  }

  Widget checkbox(String title, bool boolValue) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: boolValue,
          onChanged: (bool value) {
            setState(() {
              switch (title) {
                case "TSFWD":
                  if (value)
                    additional.add('tsfwd');
                  else
                    additional.remove('tsfwd');
                  break;
                case "SRCC":
                  if (value)
                    additional.add('srcc');
                  else
                    additional.remove('srcc');
                  break;
                case "EQVET":
                  if (value)
                    additional.add('eqvet');
                  else
                    additional.remove('eqvet');
                  break;
                case "TS":
                  if (value)
                    additional.add('ts');
                  else
                    additional.remove('ts');
                  break;
                case "ATPM":
                  if (value)
                    additional.add('atpm');
                  else
                    additional.remove('atpm');
                  break;
                case "TPL":
                  if (value)
                    additional.add('tpl');
                  else
                    additional.remove('tpl');
                  break;
                case "PA Driver":
                  if (value)
                    additional.add('pa_driver');
                  else
                    additional.remove('pa_driver');
                  break;
                case "PA Passenger":
                  if (value)
                    additional.add('pa_passenger');
                  else
                    additional.remove('pa_passenger');
                  break;
              }
              _fetchPremi();
            });
          },
        ),
        Text(
          title,
          style: TextStyle(fontSize: utils.screenAwareSize(12, context)),
        )
      ],
    );
  }

  Future _openBenefitDialog(
      List<InsuranceBenefits> benefits, String name) async {
    await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return BenefitDialog(
            insuranceName: name,
            benefits: benefits,
            selectedBenefits: selectedBenefits,
          );
        });
  }

  Future _openWorkshopDialog(int insuranceId, String insuranceName) async {
    await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return WorkshopDialog(
            insuranceId: insuranceId,
            insuranceName: insuranceName,
          );
        });
  }

  void _openSortDialog() async {
    final String result =
        await Navigator.of(context).push(MaterialPageRoute<String>(
            builder: (BuildContext context) {
              return SortDialog(
                sortByIndicator: sortByIndicator,
              );
            },
            fullscreenDialog: true));
    if (result != null) {
      List<String> splitResult = result.split('_');
      setState(() {
        sortByIndicator = result;
        sortBy = splitResult[0];
        splitResult[1] == 'asc' ? asc = true : asc = false;
      });
      setState(() {
        currPremi = CarInsurancePremi.sort(currPremi, sortBy, asc);
      });
    }
  }

  void _openSmartSystemDialog() async {
    selectedBenefits = await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return SmartSystemDialog(benefitType: 'car');
        });
    if (selectedBenefits != null) {
      setState(() {
        premis = CarInsurancePremi.smartSystem(
            widget.selectedSeries.id.toString(),
            widget.selectedPlate,
            selectedType,
            widget.selectedYear,
            additional,
            tplVal,
            paDriverVal,
            paPassengerVal,
            paPassengerSeats,
            selectedBenefits);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: utils.primaryColor,
        title: Text(
          'Car Insurance Premi',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      floatingActionButton: fab.SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22),
        curve: Curves.bounceIn,
        elevation: 10.0,
        shape: CircleBorder(),
        children: [
          fab.SpeedDialChild(
              backgroundColor: Colors.grey,
              child: Icon(Icons.sort),
              label: 'Sort',
              labelStyle:
                  TextStyle(fontSize: utils.screenAwareSize(12, context)),
              onTap: () {
                _openSortDialog();
              }),
          fab.SpeedDialChild(
              backgroundColor: Colors.grey,
              child: Icon(Icons.search),
              label: 'Smart Search',
              labelStyle:
                  TextStyle(fontSize: utils.screenAwareSize(12, context)),
              onTap: () {
                _openSmartSystemDialog();
              })
        ],
      ),
      body: Column(children: <Widget>[
        ExpansionTile(
          title: Text(
            'Choose Insurance Type or Additional Coverage',
            style: TextStyle(fontSize: utils.screenAwareSize(14, context)),
          ),
          children: <Widget>[
            Row(
              children: <Widget>[
                Radio(
                  value: 'comp',
                  groupValue: _radioValue,
                  onChanged: (String value) {
                    setState(() {
                      _changeInsuranceType(value);
                    });
                  },
                ),
                Text(
                  'COMP',
                  style:
                      TextStyle(fontSize: utils.screenAwareSize(12, context)),
                ),
                Radio(
                  value: 'tlo',
                  groupValue: _radioValue,
                  onChanged: (String value) {
                    setState(() {
                      _changeInsuranceType(value);
                    });
                  },
                ),
                Text(
                  'TLO',
                  style:
                      TextStyle(fontSize: utils.screenAwareSize(12, context)),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                checkbox("TSFWD", additional.contains('tsfwd')),
                checkbox("SRCC", additional.contains('srcc')),
                checkbox("EQVET", additional.contains('eqvet')),
                checkbox("TS", additional.contains('ts')),
              ],
            ),
            Row(
              children: <Widget>[
                selectedType == 'comp'
                    ? checkbox("ATPM", additional.contains('atpm'))
                    : Text(''),
                checkbox("TPL", additional.contains('tpl')),
                Container(
                  height: utils.screenAwareSize(30, context),
                  width: utils.screenAwareSize(105, context),
                  child: Slider(
                    onChanged: (double value) {
                      setState(() {
                        tplVal = value.toInt();
                      });
                    },
                    value: tplVal.toDouble(),
                    min: 1000000,
                    max: 10000000,
                    activeColor: utils.primaryColor,
                    inactiveColor: Colors.grey,
                    divisions: 18,
                  ),
                ),
                Text(
                  idrCurrency.format(tplVal),
                  style:
                      TextStyle(fontSize: utils.screenAwareSize(12, context)),
                )
              ],
            ),
            Row(
              children: <Widget>[
                checkbox("PA Driver", additional.contains('pa_driver')),
                Container(
                  height: utils.screenAwareSize(30, context),
                  width: utils.screenAwareSize(105, context),
                  child: Slider(
                    onChanged: (double value) {
                      setState(() {
                        paDriverVal = value.toInt();
                      });
                    },
                    value: paDriverVal.toDouble(),
                    min: 5000000,
                    max: 25000000,
                    activeColor: utils.primaryColor,
                    inactiveColor: Colors.grey,
                    divisions: 20,
                  ),
                ),
                Text(
                  idrCurrency.format(paDriverVal),
                  style:
                      TextStyle(fontSize: utils.screenAwareSize(12, context)),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                checkbox("PA Passenger", additional.contains('pa_passenger')),
                Container(
                  height: utils.screenAwareSize(25, context),
                  margin: EdgeInsets.only(left: 5.0),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey,
                          width: utils.screenAwareSize(2, context))),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      iconSize: 16.0,
                      value: paPassengerSeats,
                      onChanged: (int newValue) {
                        setState(() {
                          paPassengerSeats = newValue;
                        });
                      },
                      items: <int>[1, 2, 3, 4, 5, 6]
                          .map<DropdownMenuItem<int>>((int value) {
                        return DropdownMenuItem(
                          value: value,
                          child: Text(value.toString(),
                              style: TextStyle(
                                  fontSize:
                                      utils.screenAwareSize(12, context))),
                        );
                      }).toList(),
                    ),
                  ),
                ),
                Container(
                  height: utils.screenAwareSize(30, context),
                  width: utils.screenAwareSize(100, context),
                  child: Slider(
                    onChanged: (double value) {
                      setState(() {
                        paPassengerVal = value.toInt();
                      });
                    },
                    value: paPassengerVal.toDouble(),
                    min: 5000000,
                    max: 25000000,
                    activeColor: utils.primaryColor,
                    inactiveColor: Colors.grey,
                    divisions: 20,
                  ),
                ),
                Text(
                  idrCurrency.format(paPassengerVal),
                  style:
                      TextStyle(fontSize: utils.screenAwareSize(12, context)),
                ),
              ],
            )
          ],
        ),
        FutureBuilder(
          future: premis,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.active:
              case ConnectionState.waiting:
                return Center(
                    child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(utils.primaryColor),
                ));
              case ConnectionState.done:
                if (snapshot.hasError)
                  return Text("There was an error: ${snapshot.error}");
                currPremi = snapshot.data;
                return Expanded(
                  child: ListView.builder(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
                    itemCount: currPremi.length,
                    itemBuilder: (BuildContext context, int index) {
                      CarInsurancePremi premi = currPremi[index];
                      return Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  premi.matchValue > 0
                                      ? Text(
                                          premi.matchValue.toStringAsFixed(premi
                                                          .matchValue
                                                          .truncateToDouble() ==
                                                      premi.matchValue
                                                  ? 0
                                                  : 2) +
                                              '%',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        )
                                      : Text(''),
                                  Container(
                                    height: utils.screenAwareSize(50, context),
                                    width: utils.screenAwareSize(100, context),
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                      fit: BoxFit.fitWidth,
                                      image: NetworkImage(
                                        utils.url + premi.insuranceLogoPath,
                                      ),
                                    )),
                                  ),
                                  GestureDetector(
                                      onTap: () {
                                        _openWorkshopDialog(premi.insuranceId,
                                            premi.insuranceName);
                                      },
                                      child: Container(
                                          height: utils.screenAwareSize(
                                              35, context),
                                          width: utils.screenAwareSize(
                                              65, context),
                                          child: Card(
                                            color: utils.primaryColor,
                                            child: Center(
                                              child: Text(
                                                'Workshops',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize:
                                                        utils.screenAwareSize(
                                                            10, context)),
                                              ),
                                            ),
                                          ))),
                                  Text(
                                    '${premi.insuranceWorkshopsCount} Workshops',
                                    style:
                                        TextStyle(fontStyle: FontStyle.italic),
                                  )
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    premi.insuranceName + ' Insurance',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        decoration: TextDecoration.underline),
                                  ),
                                  Divider(),
                                  Text(
                                    'Price Detail: ',
                                    style: TextStyle(
                                        fontSize:
                                            utils.screenAwareSize(13, context)),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: premi.prices
                                        .map(
                                          (p) => Text(
                                                _priceNameNormalize(p.name) +
                                                    ': Rp. ${idrCurrency.format(p.value)}',
                                                style: TextStyle(
                                                    fontSize:
                                                        utils.screenAwareSize(
                                                            12, context)),
                                              ),
                                        )
                                        .toList(),
                                  ),
                                  Divider(),
                                  Text(
                                      'Total Price: Rp. ${idrCurrency.format(premi.totalPrice)}',
                                      style: TextStyle(
                                          fontSize: utils.screenAwareSize(
                                              13, context))),
                                  Divider(),
                                  Row(children: <Widget>[
                                    GestureDetector(
                                        onTap: () {
                                          _openBenefitDialog(
                                              premi.insuranceBenefits,
                                              premi.insuranceName);
                                        },
                                        child: Container(
                                            height: utils.screenAwareSize(
                                                35, context),
                                            width: utils.screenAwareSize(
                                                65, context),
                                            child: Card(
                                              color: utils.primaryColor,
                                              child: Center(
                                                child: Text(
                                                  'Benefits',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:
                                                          utils.screenAwareSize(
                                                              10, context)),
                                                ),
                                              ),
                                            ))),
                                    GestureDetector(
                                        onTap: () async {
                                          if (await auth.check()) {
                                            bool success =
                                                await CarInsurancePremi
                                                    .saveToWishlist(
                                                        premi,
                                                        widget.carName,
                                                        widget.selectedYear,
                                                        selectedType,
                                                        auth.token);
                                            if (success) {
                                              Scaffold.of(context)
                                                  .showSnackBar(SnackBar(
                                                content: Text(
                                                    'Item added to wishlist'),
                                                backgroundColor: Colors.green,
                                              ));
                                            } else
                                              Scaffold.of(context)
                                                  .showSnackBar(SnackBar(
                                                content: Text('Error'),
                                                backgroundColor: Colors.red,
                                              ));
                                          } else
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        LoginPage()));
                                        },
                                        child: Container(
                                            height: utils.screenAwareSize(
                                                35, context),
                                            width: utils.screenAwareSize(
                                                65, context),
                                            child: Card(
                                              color: utils.primaryColor,
                                              child: Center(
                                                child: Text(
                                                  'Add to Wishlist',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:
                                                          utils.screenAwareSize(
                                                              10, context)),
                                                ),
                                              ),
                                            ))),
                                  ]),
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                );
            }
          },
        )
      ]),
    );
  }
}
