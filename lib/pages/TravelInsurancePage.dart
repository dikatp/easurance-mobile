import 'dart:convert';

import 'package:easurance_mobile/models/TravelDestination.dart';
import 'package:easurance_mobile/pages/TravelInsurancePackagePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class TravelInsurancePage extends StatefulWidget {
  @override
  _TravelInsurancePageState createState() => _TravelInsurancePageState();
}

class _TravelInsurancePageState extends State<TravelInsurancePage> {
  List<TravelDestination> destinations = [];
  final TextEditingController _durationController = TextEditingController();
  final types = {
    "domestic": "Domestic",
    "international": "International",
  };
  final packages = {
    "individual": "Individual",
    "duo": "Duo",
    "family": "Family"
  };
  String selectedType;
  TravelDestination selectedDestination;
  String selectedPackageType;

  Future loadDestinationList() async {
    http.Response response =
        await http.get(utils.url + '/api/travel/destinations/$selectedType');
    List destinationCollection = json.decode(response.body);
    List<TravelDestination> _destinations = destinationCollection
        .map((json) => TravelDestination.fromJson(json))
        .toList();

    setState(() {
      destinations = _destinations;
    });
  }

  selectType(String value) {
    setState(() {
      selectedDestination = null;
      selectedType = value;
    });
    loadDestinationList();
  }

  selectDestination(TravelDestination value) {
    setState(() {
      selectedDestination = value;
    });
  }

  selectPackage(String value) {
    setState(() {
      selectedPackageType = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: utils.primaryColor,
        title: Text(
          'Travel Insurance',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 30.0),
        children: <Widget>[
          SizedBox(
              child: Image.asset(
            'images/logo/airplane.png',
            width: utils.screenAwareSize(100, context),
            height: utils.screenAwareSize(100, context),
          )),
          Text(
            'Fill Your Travel Plan',
            style: TextStyle(fontSize: 16.0),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Divider(
              color: utils.primaryColor,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (String newValue) {
                selectType(newValue);
              },
              value: selectedType,
              items: types.entries
                  .map<DropdownMenuItem<String>>(
                      (MapEntry<String, String> e) => DropdownMenuItem<String>(
                            value: e.key,
                            child: Text(e.value),
                          ))
                  .toList(),
              hint: Text('Choose Destination Type'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (var newValue) {
                selectDestination(newValue);
              },
              value: selectedDestination,
              items: destinations.map((value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value.name),
                );
              }).toList(),
              hint: Text('Choose Destination'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (String newValue) {
                selectPackage(newValue);
              },
              value: selectedPackageType,
              items: packages.entries
                  .map<DropdownMenuItem<String>>(
                      (MapEntry<String, String> e) => DropdownMenuItem<String>(
                            value: e.key,
                            child: Text(e.value),
                          ))
                  .toList(),
              hint: Text('Choose Package'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(55, context),
            child: TextField(
              decoration: InputDecoration(
                  hintText: "Fill Travel Duration", suffixText: 'Day(s)'),
              keyboardType: TextInputType.number,
              inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
              controller: _durationController,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(100, context),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 35.0),
            child: Builder(
              builder: (context) => RaisedButton(
                    color: utils.primaryColor,
                    textColor: Colors.white,
                    child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(
                              "Search Insurance",
                              style: TextStyle(
                                  fontSize:
                                      utils.screenAwareSize(16.0, context)),
                            )
                          ],
                        )),
                    shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    onPressed: () {
                      if (selectedDestination == null ||
                          selectedPackageType == null ||
                          _durationController.text == null) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: (Text('All Field Must be Choosen/Filled!')),
                          backgroundColor: Colors.red,
                        ));
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    TravelInsurancePackagePage(
                                      selectedDestination: selectedDestination,
                                      selectedPackageType: selectedPackageType,
                                      duration:
                                          int.parse(_durationController.text),
                                    )));
                      }
                    },
                  ),
            ),
          )
        ],
      ),
    );
  }
}
