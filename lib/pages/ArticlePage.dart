import 'package:easurance_mobile/models/Article.dart';
import 'package:easurance_mobile/pages/ArticlePageDetail.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;

class ArticlePage extends StatefulWidget {
  @override
  _ArticlePageState createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage> {
  Future<List<Article>> articles;

  @override
  void initState() {
    super.initState();

    articles = Article.loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                var _articles = Article.loadList();

                setState(() {
                  articles = _articles;
                });
              },
            )
          ],
          backgroundColor: utils.primaryColor,
          title: Text(
            'Article',
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: utils.screenAwareSize(18.0, context)),
          ),
        ),
        body: FutureBuilder(
          future: articles,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.active:
              case ConnectionState.waiting:
                return Center(
                    child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(utils.primaryColor),
                ));
              case ConnectionState.done:
                if (snapshot.hasError)
                  return Text("There was an error: ${snapshot.error}");
                var articles = snapshot.data;
                return ListView.builder(
                  padding:
                      EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
                  itemCount: articles.length,
                  itemBuilder: (BuildContext context, int index) {
                    Article article = articles[index];
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ArticleDetailPage(article: article)));
                      },
                      child: Card(
                        elevation: 8.0,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                height: utils.screenAwareSize(100, context),
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: NetworkImage(
                                            utils.url + article.thumbnail),
                                        fit: BoxFit.fitWidth)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Text(
                                article.title,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize:
                                        utils.screenAwareSize(16, context),
                                    fontWeight: FontWeight.bold),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  },
                );
            }
          },
        ));
  }
}
