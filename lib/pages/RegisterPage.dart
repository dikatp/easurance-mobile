import 'package:easurance_mobile/models/Auth.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;
import 'package:flutter/services.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final Auth auth = new Auth();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<FormFieldState> _passKey = new GlobalKey<FormFieldState>();
  String email, password, password2, name;

  bool isValidEmail(String input) {
    final RegExp regex = RegExp(
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
    return regex.hasMatch(input);
  }

  Future register() async {
    bool success = await auth.register(email, password, password2, name);
    if (success) {
      Navigator.pop(context, email);
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Error'),
        backgroundColor: Colors.red,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Sign Up',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 60.0),
          children: <Widget>[
            SizedBox(
              width: utils.screenAwareSize(140, context),
              height: utils.screenAwareSize(140, context),
              child: Image.asset(
                "images/logo/logo.png",
                color: utils.primaryColor,
                fit: BoxFit.contain,
              ),
            ),
            SizedBox(
              height: utils.screenAwareSize(20, context),
            ),
            TextFormField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.email), hintText: 'Email'),
              keyboardType: TextInputType.emailAddress,
              validator: (val) {
                if (!isValidEmail(val)) {
                  return 'Email not valid';
                }
              },
              onSaved: (value) => email = value,
            ),
            SizedBox(
              height: utils.screenAwareSize(10, context),
            ),
            TextFormField(
              key: _passKey,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.lock), hintText: 'Password'),
              obscureText: true,
              validator: (val) {
                if (val.length < 6) {
                  return 'Minimum 6 characters';
                }
              },
              onSaved: (value) => password = value,
            ),
            SizedBox(
              height: utils.screenAwareSize(10, context),
            ),
            TextFormField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.check_box),
                  hintText: 'Confirm Password'),
              obscureText: true,
              validator: (val) {
                if (val != _passKey.currentState.value) {
                  return 'Confirm password did not match';
                }
              },
              onSaved: (value) => password2 = value,
            ),
            SizedBox(
              height: utils.screenAwareSize(10, context),
            ),
            TextFormField(
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.person), hintText: 'Name'),
              validator: (val) {
                if (val.isEmpty) {
                  return "Please fill your name";
                }
              },
              onSaved: (value) => name = value,
            ),
            SizedBox(
              height: utils.screenAwareSize(60, context),
            ),
            Builder(
              builder: (context) => RaisedButton(
                  color: utils.primaryColor,
                  textColor: Colors.white,
                  child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text('Sign Up',
                              style: TextStyle(
                                  fontSize:
                                      utils.screenAwareSize(16.0, context)))
                        ],
                      )),
                  shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      register();
                    }
                  }),
            )
          ],
        ),
      ),
    );
  }
}
