import 'package:easurance_mobile/utils.dart' as utils;
import 'package:easurance_mobile/widgets/DrawerMenu.dart';
import 'package:easurance_mobile/widgets/HomeArticle.dart';
import 'package:easurance_mobile/widgets/HomeCarousel.dart';
import 'package:easurance_mobile/widgets/ProductMenu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.2,
        centerTitle: true,
        title: Image.asset(
          'images/logo/home_title.png',
          width: utils.screenAwareSize(150, context),
          height: utils.screenAwareSize(150, context),
        ),
        backgroundColor: utils.primaryColor,
      ),
      drawer: DrawerMenu(),
      body: ListView(
        children: <Widget>[
          HomeCarousel(),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Text(
              'Cari Produk Asuransi Untuk Anda',
              style: TextStyle(
                  fontSize: utils.screenAwareSize(
                    16,
                    context,
                  ),
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
            child: Divider(
              color: utils.primaryColor,
            ),
          ),
          ProductMenu(),
          Divider(
            color: utils.primaryColor,
          ),
          HomeArticle()
        ],
      ),
    );
  }
}
