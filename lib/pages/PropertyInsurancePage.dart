import 'dart:convert';

import 'package:easurance_mobile/models/PropertyCity.dart';
import 'package:easurance_mobile/models/PropertyProvince.dart';
import 'package:easurance_mobile/pages/PropertyInsurancePremiPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;
import 'package:flutter_masked_text/flutter_masked_text.dart' as mask;

class PropertyInsurancePage extends StatefulWidget {
  @override
  _PropertyInsurancePageState createState() => _PropertyInsurancePageState();
}

class _PropertyInsurancePageState extends State<PropertyInsurancePage> {
  List<PropertyProvince> provinces = [];
  List<PropertyCity> cities = [];
  final flexas = {
    "1": "Kelas 1 Bersifat Tahan Api (> 80% Baja)",
    "2": "Kelas 2 Relatif Tahan Api (80% Baja 20% Kayu)",
    "3": "Kelas 3 Selain Kelas 1 & 2"
  };
  PropertyProvince selectedProvince;
  PropertyCity selectedCity;
  String selectedFlexas;
  var valueController =
      new mask.MoneyMaskedTextController(precision: 0, decimalSeparator: '');
  final TextEditingController _addressController = TextEditingController();

  Future loadProvinceList() async {
    http.Response response =
        await http.get(utils.url + '/api/property/provinces');
    List provinceCollection = json.decode(response.body);
    List<PropertyProvince> _provinces = provinceCollection
        .map((json) => PropertyProvince.fromJson(json))
        .toList();

    setState(() {
      provinces = _provinces;
    });
  }

  Future loadCityList() async {
    http.Response response = await http.get(
        utils.url + '/api/property/provinces/${selectedProvince.id}/cities');
    List cityCollection = json.decode(response.body);
    List<PropertyCity> _cities =
        cityCollection.map((json) => PropertyCity.fromJson(json)).toList();

    setState(() {
      cities = _cities;
    });
  }

  @override
  void initState() {
    loadProvinceList();
    super.initState();
  }

  void selectProvince(PropertyProvince value) {
    setState(() {
      selectedCity = null;
      selectedProvince = value;
    });
    loadCityList();
  }

  void selectCity(PropertyCity value) {
    setState(() {
      selectedCity = value;
    });
  }

  void selectFlexas(String value) {
    setState(() {
      selectedFlexas = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: utils.primaryColor,
        title: Text(
          'Property Insurance',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
        children: <Widget>[
          SizedBox(
              child: Image.asset(
            'images/logo/house.png',
            width: utils.screenAwareSize(100, context),
            height: utils.screenAwareSize(100, context),
          )),
          Text(
            'Fill Your Property Data',
            style: TextStyle(fontSize: 16.0),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Divider(
              color: utils.primaryColor,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (var newValue) {
                selectProvince(newValue);
              },
              value: selectedProvince,
              items: provinces.map((value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value.name),
                );
              }).toList(),
              hint: Text('Choose Province'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (var newValue) {
                selectCity(newValue);
              },
              value: selectedCity,
              items: cities.map((value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value.name),
                );
              }).toList(),
              hint: Text('Choose City'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(55, context),
            child: TextField(
              controller: _addressController,
              decoration: InputDecoration(
                hintText: "Your Home Address",
              ),
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(60, context),
            child: DropdownButton(
              onChanged: (String newValue) {
                selectFlexas(newValue);
              },
              value: selectedFlexas,
              items: flexas.entries
                  .map<DropdownMenuItem<String>>(
                      (MapEntry<String, String> e) => DropdownMenuItem<String>(
                            value: e.key,
                            child: Text(e.value),
                          ))
                  .toList(),
              hint: Text('Choose Property Class'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(55, context),
            child: TextField(
              controller: valueController,
              decoration: InputDecoration(
                  hintText: "Your Property Value", prefixText: 'Rp. '),
              keyboardType: TextInputType.number,
              inputFormatters: [
                BlacklistingTextInputFormatter(RegExp('[\\-|\\ ]'))
              ],
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(70, context),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40.0),
            child: Builder(
              builder: (context) => RaisedButton(
                    color: utils.primaryColor,
                    textColor: Colors.white,
                    child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(
                              "Search Insurance",
                              style: TextStyle(
                                  fontSize:
                                      utils.screenAwareSize(16.0, context)),
                            )
                          ],
                        )),
                    shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    onPressed: () {
                      if (selectedCity == null ||
                          selectedFlexas == null ||
                          valueController.text == null ||
                          _addressController.text == null) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: (Text('All Field Must be Choosen/Filled!')),
                          backgroundColor: Colors.red,
                        ));
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    PropertyInsurancePremiPage(
                                      selectedProvince: selectedProvince,
                                      selectedCity: selectedCity,
                                      address: _addressController.text,
                                      selectedFlexas: selectedFlexas,
                                      value:
                                          valueController.numberValue.toInt(),
                                    )));
                      }
                    },
                  ),
            ),
          )
        ],
      ),
    );
  }
}
