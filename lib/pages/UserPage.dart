import 'package:easurance_mobile/models/Auth.dart';
import 'package:easurance_mobile/models/User.dart';
import 'package:easurance_mobile/pages/WishlistPage.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  final Auth auth = new Auth();
  Future<User> user;

  @override
  void initState() {
    user = User.load(auth.token);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: utils.primaryColor,
        title: Text(
          'My Account',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      body: FutureBuilder(
        future: user,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(
                  child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(utils.primaryColor),
              ));
            case ConnectionState.active:
            case ConnectionState.done:
              if (snapshot.hasError)
                return Text("There was an error: ${snapshot.error}");
              User user = snapshot.data;
              return Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        'Name:',
                        style: TextStyle(
                            fontSize: utils.screenAwareSize(18, context)),
                      ),
                    ),
                    Card(
                        color: utils.backgroundColor,
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text(
                            user.name,
                            style: TextStyle(
                                fontSize: utils.screenAwareSize(18, context)),
                          ),
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        'Email:',
                        style: TextStyle(
                            fontSize: utils.screenAwareSize(18, context)),
                      ),
                    ),
                    Card(
                        color: utils.backgroundColor,
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Text(
                            user.email,
                            style: TextStyle(
                                fontSize: utils.screenAwareSize(18, context)),
                          ),
                        )),
                    SizedBox(
                      height: utils.screenAwareSize(150, context),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        RaisedButton(
                          color: utils.primaryColor,
                          child: Text(
                            'Wishlist',
                            style: TextStyle(color: Colors.white),
                          ),
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => WishlistPage()));
                          },
                        ),
                        RaisedButton(
                          color: Colors.red,
                          child: Text('Logout'),
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0)),
                          onPressed: () {
                            auth.logout();
                            Navigator.pop(context);
                          },
                        )
                      ],
                    )
                  ]));
          }
        },
      ),
    );
  }
}
