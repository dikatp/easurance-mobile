import 'dart:convert';

import 'package:easurance_mobile/models/CarBrand.dart';
import 'package:easurance_mobile/models/CarPlate.dart';
import 'package:easurance_mobile/models/CarSeries.dart';
import 'package:easurance_mobile/models/CarType.dart';
import 'package:easurance_mobile/pages/CarInsurancePremiPage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class CarInsurancePage extends StatefulWidget {
  @override
  _CarInsurancePageState createState() => _CarInsurancePageState();
}

class _CarInsurancePageState extends State<CarInsurancePage> {
  List<String> years = [
    "2019",
    "2018",
    "2017",
    "2016",
    "2015",
    "2014",
    "2013",
    "2012",
    "2010",
    "2009",
  ];
  List<CarBrand> brands = [];
  List<CarType> types = [];
  List<CarSeries> series = [];
  List<CarPlate> plates = [];

  CarBrand selectedBrand;
  CarType selectedType;
  CarSeries selectedSeries;
  CarPlate selectedPlate;

  String selectedYear;

  Future loadBrandList() async {
    http.Response response =
        await http.get(utils.url + '/api/car/brands/$selectedYear');
    List brandCollection = json.decode(response.body);
    List<CarBrand> _brands =
        brandCollection.map((json) => CarBrand.fromJson(json)).toList();

    setState(() {
      brands = _brands;
    });
  }

  Future loadTypeList() async {
    http.Response response =
        await http.get(utils.url + '/api/car/brands/${selectedBrand.id}/types');
    List typeCollection = json.decode(response.body);
    List<CarType> _types =
        typeCollection.map((json) => CarType.fromJson(json)).toList();

    setState(() {
      types = _types;
    });
  }

  Future loadSeriesList() async {
    http.Response response =
        await http.get(utils.url + '/api/car/types/${selectedType.id}/series');
    List seriesCollection = json.decode(response.body);
    List<CarSeries> _series =
        seriesCollection.map((json) => CarSeries.fromJson(json)).toList();

    setState(() {
      series = _series;
    });
  }

  Future loadPlateList() async {
    http.Response response = await http.get(utils.url + '/api/car/plates');
    List plateCollection = json.decode(response.body);
    List<CarPlate> _plates =
        plateCollection.map((json) => CarPlate.fromJson(json)).toList();

    setState(() {
      plates = _plates;
    });
  }

  @override
  void initState() {
    loadPlateList();
    super.initState();
  }

  void selectYear(String value) {
    setState(() {
      selectedBrand = null;
      selectedType = null;
      selectedSeries = null;
      brands = [];
      types = [];
      series = [];
      selectedYear = value;
    });
    loadBrandList();
  }

  void selectBrand(CarBrand value) {
    setState(() {
      selectedType = null;
      selectedSeries = null;
      types = [];
      series = [];
      selectedBrand = value;
    });
    loadTypeList();
  }

  void selectType(CarType value) {
    setState(() {
      selectedSeries = null;
      series = [];
      selectedType = value;
    });
    loadSeriesList();
  }

  void selectSeries(CarSeries value) {
    setState(() {
      selectedSeries = value;
    });
  }

  void selectPlate(CarPlate value) {
    setState(() {
      selectedPlate = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: utils.primaryColor,
        title: Text(
          'Car Insurance',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 30.0),
        children: <Widget>[
          SizedBox(
              child: Image.asset(
            'images/logo/car.png',
            width: utils.screenAwareSize(100, context),
            height: utils.screenAwareSize(100, context),
          )),
          Text(
            'Fill Your Car Data',
            style: TextStyle(fontSize: 16.0),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Divider(
              color: utils.primaryColor,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (String newValue) {
                selectYear(newValue);
              },
              value: selectedYear,
              items: years.map((String value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              hint: Text('Choose Year'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (var newValue) {
                selectBrand(newValue);
              },
              value: selectedBrand,
              items: brands.map((value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value.name),
                );
              }).toList(),
              hint: Text('Choose Brand'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (var newValue) {
                selectType(newValue);
              },
              value: selectedType,
              items: types.map((value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value.name),
                );
              }).toList(),
              hint: Text('Choose Type'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (var newValue) {
                selectSeries(newValue);
              },
              value: selectedSeries,
              items: series.map((value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value.name),
                );
              }).toList(),
              hint: Text('Choose Series'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(35, context),
            child: DropdownButton(
              onChanged: (var newValue) {
                selectPlate(newValue);
              },
              value: selectedPlate,
              items: plates.map((value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(value.plate),
                );
              }).toList(),
              hint: Text('Choose Car Plate'),
              isExpanded: true,
            ),
          ),
          SizedBox(
            height: utils.screenAwareSize(100, context),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40.0),
            child: Builder(
              builder: (context) => RaisedButton(
                    color: utils.primaryColor,
                    textColor: Colors.white,
                    child: Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(
                              "Search Insurance",
                              style: TextStyle(
                                  fontSize:
                                      utils.screenAwareSize(16.0, context)),
                            )
                          ],
                        )),
                    shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    onPressed: () {
                      if (selectedSeries == null || selectedPlate == null) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: (Text('All Field Must be Choosen!')),
                          backgroundColor: Colors.red,
                        ));
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CarInsurancePremiPage(
                                      carName: selectedBrand.name +
                                          ' ' +
                                          selectedType.name +
                                          ' ' +
                                          selectedSeries.name,
                                      selectedPlate:
                                          selectedPlate.regionCode.toString(),
                                      selectedSeries: selectedSeries,
                                      selectedYear: selectedYear,
                                    )));
                      }
                    },
                  ),
            ),
          )
        ],
      ),
    );
  }
}
