import 'package:easurance_mobile/models/Auth.dart';
import 'package:easurance_mobile/models/InsuranceBenefit.dart';
import 'package:easurance_mobile/models/TravelDestination.dart';
import 'package:easurance_mobile/models/TravelInsurancePackage.dart';
import 'package:easurance_mobile/pages/LoginPage.dart';
import 'package:easurance_mobile/widgets/BenefitDialog.dart';
import 'package:easurance_mobile/widgets/SmartSystemDialog.dart';
import 'package:easurance_mobile/widgets/SortDialog.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'package:easurance_mobile/utils.dart' as utils;
import 'package:flutter_speed_dial/flutter_speed_dial.dart' as fab;

class TravelInsurancePackagePage extends StatefulWidget {
  final TravelDestination selectedDestination;
  final String selectedPackageType;
  final int duration;

  const TravelInsurancePackagePage(
      {Key key,
      this.selectedDestination,
      this.selectedPackageType,
      this.duration})
      : super(key: key);

  @override
  _TravelInsurancePremiPageState createState() =>
      _TravelInsurancePremiPageState();
}

class _TravelInsurancePremiPageState extends State<TravelInsurancePackagePage> {
  final Auth auth = new Auth();
  final idrCurrency = new intl.NumberFormat("#,##0", "id_ID");
  Future<List<TravelInsurancePackage>> packages;
  String sortBy, sortByIndicator;
  bool asc;
  List<TravelInsurancePackage> currPackage;

  @override
  void initState() {
    sortBy = "name";
    sortByIndicator = "name_asc";
    asc = true;
    super.initState();
    _fetchPackage();
  }

  String _priceNameNormalize(String value) {
    String nameNormalize;
    List<String> splitList = value.split('_');
    if (splitList.length > 1) {
      if (splitList[1].contains('comp')) {
        nameNormalize = splitList[0].toUpperCase();
      } else {
        for (var i = 0; i < splitList.length; i++) {
          splitList[i] =
              splitList[i][0].toUpperCase() + splitList[i].substring(1);
        }
        nameNormalize = splitList.join(' ');
      }
    } else {
      nameNormalize = splitList[0].toUpperCase();
    }
    return nameNormalize;
  }

  void _fetchPackage() {
    setState(() {
      packages = TravelInsurancePackage.loadList(widget.selectedDestination,
          widget.selectedPackageType, widget.duration);
    });
  }

  Future _openBenefitDialog(
      List<InsuranceBenefits> benefits, String name) async {
    await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return BenefitDialog(insuranceName: name, benefits: benefits);
        });
  }

  void _openSortDialog() async {
    final String result =
        await Navigator.of(context).push(MaterialPageRoute<String>(
            builder: (BuildContext context) {
              return SortDialog(
                sortByIndicator: sortByIndicator,
              );
            },
            fullscreenDialog: true));
    if (result != null) {
      List<String> splitResult = result.split('_');
      setState(() {
        sortByIndicator = result;
        sortBy = splitResult[0];
        splitResult[1] == 'asc' ? asc = true : asc = false;
      });
      setState(() {
        currPackage = TravelInsurancePackage.sort(currPackage, sortBy, asc);
      });
    }
  }

  void _openSmartSystemDialog() async {
    final List<InsuranceBenefits> result = await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return SmartSystemDialog(benefitType: 'travel');
        });
    if (result != null) {
      setState(() {
        packages = TravelInsurancePackage.smartSystem(
            widget.selectedDestination,
            widget.selectedPackageType,
            widget.duration,
            result);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: utils.primaryColor,
        title: Text(
          'Travel Insurance Package',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: utils.screenAwareSize(18.0, context)),
        ),
      ),
      floatingActionButton: fab.SpeedDial(
        animatedIcon: AnimatedIcons.menu_close,
        animatedIconTheme: IconThemeData(size: 22),
        curve: Curves.bounceIn,
        elevation: 10.0,
        shape: CircleBorder(),
        children: [
          fab.SpeedDialChild(
              backgroundColor: Colors.grey,
              child: Icon(Icons.sort),
              label: 'Sort',
              labelStyle:
                  TextStyle(fontSize: utils.screenAwareSize(12, context)),
              onTap: () {
                _openSortDialog();
              }),
          fab.SpeedDialChild(
              backgroundColor: Colors.grey,
              child: Icon(Icons.search),
              label: 'Smart Search',
              labelStyle:
                  TextStyle(fontSize: utils.screenAwareSize(12, context)),
              onTap: () {
                _openSmartSystemDialog();
              })
        ],
      ),
      body: FutureBuilder(
        future: packages,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(
                  child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(utils.primaryColor),
              ));
            case ConnectionState.active:
            case ConnectionState.done:
              if (snapshot.hasError)
                return Text("There was an error: ${snapshot.error}");
              currPackage = snapshot.data;
              return ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
                itemCount: currPackage.length,
                itemBuilder: (BuildContext context, int index) {
                  TravelInsurancePackage package = currPackage[index];
                  return Card(
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              package.matchValue > 0
                                  ? Text(
                                      package.matchValue.toStringAsFixed(package
                                                      .matchValue
                                                      .truncateToDouble() ==
                                                  package.matchValue
                                              ? 0
                                              : 2) +
                                          '%',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  : Text(''),
                              Text(
                                package.package + ' Package',
                                style: TextStyle(
                                    fontSize:
                                        utils.screenAwareSize(15, context),
                                    fontWeight: FontWeight.bold),
                              ),
                              Container(
                                height: utils.screenAwareSize(50, context),
                                width: utils.screenAwareSize(100, context),
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                  fit: BoxFit.fitWidth,
                                  image: NetworkImage(
                                    utils.url + package.insuranceLogoPath,
                                  ),
                                )),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                package.insuranceName + ' Insurance',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              ),
                              Divider(),
                              Text(
                                'Price Detail: ',
                                style: TextStyle(
                                    fontSize:
                                        utils.screenAwareSize(13, context)),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: package.prices
                                    .map(
                                      (p) => Text(
                                            _priceNameNormalize(p.name) +
                                                ': Rp. ${idrCurrency.format(p.value)}',
                                            style: TextStyle(
                                                fontSize: utils.screenAwareSize(
                                                    12, context)),
                                          ),
                                    )
                                    .toList(),
                              ),
                              Divider(),
                              Text(
                                  'Total Price: Rp. ${idrCurrency.format(package.totalPrice)}',
                                  style: TextStyle(
                                      fontSize:
                                          utils.screenAwareSize(13, context))),
                              Divider(),
                              Row(
                                children: <Widget>[
                                  GestureDetector(
                                      onTap: () {
                                        _openBenefitDialog(
                                            package.insuranceBenefits,
                                            package.insuranceName);
                                      },
                                      child: Container(
                                          height: utils.screenAwareSize(
                                              35, context),
                                          width: utils.screenAwareSize(
                                              65, context),
                                          child: Card(
                                            color: utils.primaryColor,
                                            child: Center(
                                              child: Text(
                                                'Benefits',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize:
                                                        utils.screenAwareSize(
                                                            10, context)),
                                              ),
                                            ),
                                          ))),
                                  GestureDetector(
                                      onTap: () async {
                                        if (await auth.check()) {
                                          bool success =
                                              await TravelInsurancePackage
                                                  .saveToWishlist(
                                                      package,
                                                      widget
                                                          .selectedDestination,
                                                      widget.duration,
                                                      auth.token);
                                          if (success) {
                                            Scaffold.of(context)
                                                .showSnackBar(SnackBar(
                                              content: Text(
                                                  'Item added to wishlist'),
                                              backgroundColor: Colors.green,
                                            ));
                                          } else
                                            Scaffold.of(context)
                                                .showSnackBar(SnackBar(
                                              content: Text('Error'),
                                              backgroundColor: Colors.red,
                                            ));
                                        } else
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      LoginPage()));
                                      },
                                      child: Container(
                                          height: utils.screenAwareSize(
                                              35, context),
                                          width: utils.screenAwareSize(
                                              65, context),
                                          child: Card(
                                            color: utils.primaryColor,
                                            child: Center(
                                              child: Text(
                                                'Add to Wishlist',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize:
                                                        utils.screenAwareSize(
                                                            10, context)),
                                              ),
                                            ),
                                          )))
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                },
              );
          }
        },
      ),
    );
  }
}
