import 'package:easurance_mobile/models/CarWorkshop.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;

class WorkshopDialog extends StatefulWidget {
  final int insuranceId;
  final String insuranceName;

  const WorkshopDialog({Key key, this.insuranceId, this.insuranceName})
      : super(key: key);

  @override
  _WorkshopDialogState createState() => _WorkshopDialogState();
}

class _WorkshopDialogState extends State<WorkshopDialog> {
  Future<List<CarWorkshop>> workshops;

  @override
  void initState() {
    workshops = CarWorkshop.loadList(widget.insuranceId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        widget.insuranceName + ' Insurance Workshops',
        textAlign: TextAlign.center,
      ),
      content: FutureBuilder(
        future: workshops,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.active:
            case ConnectionState.waiting:
              return Center(
                  child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(utils.primaryColor),
              ));
            case ConnectionState.done:
              if (snapshot.hasError)
                return Text("There was an error: ${snapshot.error}");
              var workshops = snapshot.data;
              return ListView.separated(
                separatorBuilder: (context, index) => Divider(
                      color: utils.primaryColor,
                    ),
                itemCount: workshops.length,
                itemBuilder: (BuildContext context, int index) {
                  CarWorkshop workshop = workshops[index];
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          workshop.name,
                          style: TextStyle(
                              fontSize: utils.screenAwareSize(12, context)),
                        ),
                        Text(
                          workshop.address,
                          style: TextStyle(
                              fontSize: utils.screenAwareSize(12, context)),
                        ),
                        Text(
                          workshop.city,
                          style: TextStyle(
                              fontSize: utils.screenAwareSize(12, context)),
                        ),
                        Text(
                          workshop.phone,
                          style: TextStyle(
                              fontSize: utils.screenAwareSize(12, context)),
                        ),
                      ],
                    ),
                  );
                },
              );
          }
        },
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Close'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
