import 'package:easurance_mobile/pages/CarInsurancePage.dart';
import 'package:easurance_mobile/pages/PropertyInsurancePage.dart';
import 'package:easurance_mobile/pages/TravelInsurancePage.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;

class ProductMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: 3,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CarInsurancePage()));
            },
            child: Card(
              color: utils.primaryColor,
              elevation: utils.screenAwareSize(3.0, context),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Image.asset(
                      'images/logo/car.png',
                      width: utils.screenAwareSize(60.0, context),
                      height: utils.screenAwareSize(60.0, context),
                    ),
                  ),
                  Text(
                    'Mobil',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: utils.backgroundColor,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PropertyInsurancePage()));
            },
            child: Card(
              color: utils.backgroundColor,
              elevation: utils.screenAwareSize(3.0, context),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Image.asset(
                      'images/logo/house.png',
                      width: utils.screenAwareSize(60.0, context),
                      height: utils.screenAwareSize(60.0, context),
                    ),
                  ),
                  Text(
                    'Rumah',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: utils.primaryColor,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TravelInsurancePage()));
            },
            child: Card(
              color: utils.primaryColor,
              elevation: utils.screenAwareSize(3.0, context),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Image.asset(
                      'images/logo/airplane.png',
                      width: utils.screenAwareSize(60.0, context),
                      height: utils.screenAwareSize(60.0, context),
                    ),
                  ),
                  Text(
                    'Perjalanan',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: utils.backgroundColor,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
