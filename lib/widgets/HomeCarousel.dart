import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;
import 'package:carousel_pro/carousel_pro.dart';

class HomeCarousel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: utils.screenAwareSize(200, context),
      child: new Carousel(
        boxFit: BoxFit.cover,
        images: [
          AssetImage('images/carousel/slide1.jpg'),
          AssetImage('images/carousel/slide2.jpg'),
          AssetImage('images/carousel/slide3.jpg'),
        ],
        autoplay: true,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
        dotSize: utils.screenAwareSize(4.0, context),
        indicatorBgPadding: utils.screenAwareSize(3.0, context),
      ),
    );
  }
}
