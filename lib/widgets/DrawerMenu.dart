import 'package:easurance_mobile/pages/ArticlePage.dart';
import 'package:easurance_mobile/pages/FaqPage.dart';
import 'package:easurance_mobile/pages/LoginPage.dart';
import 'package:easurance_mobile/pages/UserPage.dart';
import 'package:easurance_mobile/pages/WishlistPage.dart';
import 'package:flutter/material.dart';
import '../models/Auth.dart';

class DrawerMenu extends StatefulWidget {
  @override
  _DrawerMenuState createState() => _DrawerMenuState();
}

class _DrawerMenuState extends State<DrawerMenu> {
  final Auth auth = new Auth();
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Padding(
        padding: const EdgeInsets.only(top: 15.0),
        child: Column(
          children: <Widget>[
            ListTile(
              onTap: () async {
                if (await auth.check())
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => UserPage()));
                else
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LoginPage(
                              intendedRoute: MaterialPageRoute(
                                  builder: (context) => UserPage()))));
              },
              leading: Icon(Icons.person),
              title: Text('My Account'),
            ),
            ListTile(
              onTap: () async {
                if (await auth.check())
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => WishlistPage()));
                else
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LoginPage(
                              intendedRoute: MaterialPageRoute(
                                  builder: (context) => WishlistPage()))));
              },
              leading: Icon(Icons.favorite),
              title: Text('Wishlist'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ArticlePage()));
              },
              leading: Icon(Icons.book),
              title: Text('Article'),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => FaqPage()));
              },
              leading: Icon(Icons.question_answer),
              title: Text('FAQ'),
            ),
          ],
        ),
      ),
    );
  }
}
