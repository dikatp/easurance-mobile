import 'package:easurance_mobile/models/Auth.dart';
import 'package:easurance_mobile/models/TravelWishlist.dart';
import 'package:easurance_mobile/widgets/DeleteDialog.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;
import 'package:intl/intl.dart' as intl;
import 'package:flutter_slidable/flutter_slidable.dart' as fs;

class TravelWishlistTab extends StatefulWidget {
  @override
  _TravelWishlistTabState createState() => _TravelWishlistTabState();
}

class _TravelWishlistTabState extends State<TravelWishlistTab> {
  Auth auth = new Auth();
  Future<List<TravelWishlist>> wishlists;
  final idrCurrency = new intl.NumberFormat("#,##0", "id_ID");

  @override
  void initState() {
    wishlists = TravelWishlist.loadList(auth.token);
    super.initState();
  }

  String _priceNameNormalize(String value) {
    String nameNormalize;
    List<String> splitList = value.split('_');
    if (splitList.length > 1) {
      if (splitList[1].contains('comp')) {
        nameNormalize = splitList[0].toUpperCase();
      } else {
        for (var i = 0; i < splitList.length; i++) {
          splitList[i] =
              splitList[i][0].toUpperCase() + splitList[i].substring(1);
        }
        nameNormalize = splitList.join(' ');
      }
    } else {
      nameNormalize = splitList[0].toUpperCase();
    }
    return nameNormalize;
  }

  void _openDeleteDialog(int wishlistId) async {
    bool result = await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return DeleteDialog();
        });
    if (result) {
      bool success = await TravelWishlist.delete(wishlistId, auth.token);
      setState(() {
        wishlists = TravelWishlist.loadList(auth.token);
      });
      if (success) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Wishlist Deleted'),
          backgroundColor: Colors.green,
        ));
      } else
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Error'),
          backgroundColor: Colors.red,
        ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: wishlists,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(utils.primaryColor),
            ));
          case ConnectionState.done:
            if (snapshot.hasError)
              return Text("There was an error: ${snapshot.error}");
            var wishlists = snapshot.data;
            return ListView.builder(
              itemCount: wishlists.length,
              itemBuilder: (BuildContext context, int index) {
                TravelWishlist wishlist = wishlists[index];
                return fs.Slidable(
                  actionPane: fs.SlidableDrawerActionPane(),
                  actionExtentRatio: 0.2,
                  child: Card(
                    elevation: 3,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                wishlist.insuranceName + ' Insurance',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              ),
                              Divider(),
                              Text(
                                'Package: ' + wishlist.packageName,
                                style: TextStyle(
                                    fontSize:
                                        utils.screenAwareSize(13, context)),
                              ),
                              Text('Destination: ' + wishlist.destination,
                                  style: TextStyle(
                                      fontSize:
                                          utils.screenAwareSize(13, context))),
                              Text('Day: ' + wishlist.day.toString(),
                                  style: TextStyle(
                                      fontSize:
                                          utils.screenAwareSize(13, context))),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Price Detail: ',
                                style: TextStyle(
                                    fontSize:
                                        utils.screenAwareSize(13, context),
                                    fontWeight: FontWeight.bold,
                                    decoration: TextDecoration.underline),
                              ),
                              Divider(),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: wishlist.prices
                                    .map(
                                      (p) => Text(
                                            _priceNameNormalize(p.name) +
                                                ': Rp. ${idrCurrency.format(p.value)}',
                                            style: TextStyle(
                                                fontSize: utils.screenAwareSize(
                                                    13, context)),
                                          ),
                                    )
                                    .toList(),
                              ),
                              Divider(),
                              Text(
                                  'Total Price: Rp. ${idrCurrency.format(wishlist.totalPrice)}',
                                  style: TextStyle(
                                      fontSize:
                                          utils.screenAwareSize(13, context))),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  secondaryActions: <Widget>[
                    fs.IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        _openDeleteDialog(wishlist.id);
                      },
                    )
                  ],
                );
              },
            );
        }
      },
    );
  }
}
