import 'package:easurance_mobile/models/InsuranceBenefit.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;

class BenefitDialog extends StatelessWidget {
  final String insuranceName;
  final List<InsuranceBenefits> benefits;
  final List<InsuranceBenefits> selectedBenefits;

  const BenefitDialog(
      {Key key, this.insuranceName, this.benefits, this.selectedBenefits})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        insuranceName + ' Insurance Benefits',
        textAlign: TextAlign.center,
      ),
      content: ListView(
        padding: EdgeInsets.all(10.0),
        children: benefits
            .map((b) => Card(
                elevation: 8.0,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    b.name,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: utils.screenAwareSize(14, context),
                        color: selectedBenefits != null
                            ? selectedBenefits.contains(b)
                                ? utils.primaryColor
                                : Colors.black
                            : Colors.black),
                  ),
                )))
            .toList(),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Close'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
