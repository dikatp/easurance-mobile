import 'package:easurance_mobile/models/Article.dart';
import 'package:easurance_mobile/pages/ArticlePageDetail.dart';
import 'package:easurance_mobile/utils.dart' as utils;
import 'package:flutter/material.dart';

class HomeArticle extends StatefulWidget {
  @override
  _HomeArticleState createState() => _HomeArticleState();
}

class _HomeArticleState extends State<HomeArticle> {
  Future<List<Article>> articles;

  @override
  void initState() {
    super.initState();

    articles = Article.random();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: articles,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return Center(
                child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(utils.primaryColor),
            ));
          case ConnectionState.done:
            if (snapshot.hasError)
              return Text("There was an error: ${snapshot.error}");
            var articles = snapshot.data;
            return GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, childAspectRatio: 1.5),
              shrinkWrap: true,
              itemCount: articles.length,
              itemBuilder: (BuildContext context, int index) {
                Article article = articles[index];
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ArticleDetailPage(article: article)));
                  },
                  child: Card(
                    child: Stack(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                      utils.url + article.thumbnail),
                                  fit: BoxFit.fill,
                                  colorFilter: ColorFilter.mode(
                                      Colors.black.withOpacity(0.3),
                                      BlendMode.dstATop))),
                        ),
                        Center(
                          child: Text(
                            article.title,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: utils.screenAwareSize(16, context),
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
        }
      },
    );
  }
}
