import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;

class SortDialog extends StatelessWidget {
  final String sortByIndicator;
  final options = const [
    {
      'title': 'Insurance Name A-Z',
      'sortBy': 'name_asc',
    },
    {
      'title': 'Insurance Name Z-A',
      'sortBy': 'name_desc',
    },
    {
      'title': 'Total Price Lowest',
      'sortBy': 'total_asc',
    },
    {
      'title': 'Total Price Highest',
      'sortBy': 'total_desc',
    },
  ];

  const SortDialog({Key key, this.sortByIndicator}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        'Sort',
        style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: utils.screenAwareSize(18.0, context)),
      )),
      body: ListView.separated(
        padding: EdgeInsets.symmetric(vertical: 20),
        separatorBuilder: (BuildContext context, int index) => Divider(),
        itemCount: options.length,
        itemBuilder: (BuildContext context, int index) {
          var option = options[index];

          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              child: Text(
                option['title'],
                style: TextStyle(
                    color: option['sortBy'] == sortByIndicator
                        ? utils.primaryColor
                        : Colors.black),
              ),
              onTap: () {
                Navigator.of(context).pop(option['sortBy']);
              },
            ),
          );
        },
      ),
    );
  }
}
