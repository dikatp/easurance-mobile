import 'package:easurance_mobile/models/InsuranceBenefit.dart';
import 'package:flutter/material.dart';
import 'package:easurance_mobile/utils.dart' as utils;

class SmartSystemDialog extends StatefulWidget {
  final String benefitType;

  const SmartSystemDialog({Key key, this.benefitType}) : super(key: key);

  @override
  _SmartSystemDialogState createState() => _SmartSystemDialogState();
}

class _SmartSystemDialogState extends State<SmartSystemDialog> {
  Future<List<InsuranceBenefits>> benefits;
  List<InsuranceBenefits> selectedBenefits = [];

  @override
  void initState() {
    benefits = InsuranceBenefits.loadBenefits(widget.benefitType);
    super.initState();
  }

  Widget checkbox(InsuranceBenefits benefit, bool boolValue) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: boolValue,
          onChanged: (bool value) {
            setState(() {
              if (value)
                selectedBenefits.add(benefit);
              else
                selectedBenefits.remove(benefit);
            });
          },
        ),
        Expanded(
          child: Text(
            benefit.name,
            style: TextStyle(fontSize: utils.screenAwareSize(12, context)),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Choose Benefits That You Desired',
        textAlign: TextAlign.center,
      ),
      content: FutureBuilder(
        future: benefits,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(
                  child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(utils.primaryColor),
              ));
            case ConnectionState.active:
            case ConnectionState.done:
              if (snapshot.hasError)
                return Text("There was an error: ${snapshot.error}");
              var benefits = snapshot.data;
              return ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                        color: utils.primaryColor,
                      ),
                  itemCount: benefits.length,
                  itemBuilder: (BuildContext context, int index) {
                    InsuranceBenefits benefit = benefits[index];
                    return checkbox(
                        benefit, selectedBenefits.contains(benefit));
                  });
          }
        },
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Use Smart System'),
          onPressed: () {
            Navigator.of(context).pop(selectedBenefits);
          },
        ),
        FlatButton(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
