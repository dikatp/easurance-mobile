import 'package:easurance_mobile/pages/HomePage.dart';
import 'package:flutter/material.dart';

void main() => runApp(Easurance());

class Easurance extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.green, accentColor: Colors.green),
      title: 'Easurance',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
