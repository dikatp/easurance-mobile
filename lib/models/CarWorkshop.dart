import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class CarWorkshop {
  int id;
  String name;
  String address;
  String city;
  String phone;
  String createdAt;
  String updatedAt;

  CarWorkshop(
      {this.id,
      this.name,
      this.address,
      this.city,
      this.phone,
      this.createdAt,
      this.updatedAt});

  CarWorkshop.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address = json['address'];
    city = json['city'];
    phone = json['phone'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['address'] = this.address;
    data['city'] = this.city;
    data['phone'] = this.phone;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;

    return data;
  }

  static Future<List<CarWorkshop>> loadList(int insuranceId) async {
    http.Response response =
        await http.get(utils.url + '/api/car-insurance/$insuranceId/workshops');
    List collection = json.decode(response.body);
    List<CarWorkshop> _workshops =
        collection.map((json) => CarWorkshop.fromJson(json)).toList();

    return _workshops;
  }
}
