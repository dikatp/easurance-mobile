class CarPlate {
  int id;
  String plate;
  int regionCode;
  String createdAt;
  String updatedAt;

  CarPlate(
      {this.id, this.plate, this.regionCode, this.createdAt, this.updatedAt});

  CarPlate.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    plate = json['plate'];
    regionCode = json['region_code'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['plate'] = this.plate;
    data['region_code'] = this.regionCode;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
