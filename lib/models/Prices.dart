class Prices {
  String name;
  int value;

  Prices({this.name, this.value});

  Prices.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    value = json['value'] is int ? json['value'] : int.tryParse(json['value']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['value'] = this.value;
    return data;
  }
}
