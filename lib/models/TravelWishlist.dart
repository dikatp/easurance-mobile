import 'package:easurance_mobile/models/Prices.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class TravelWishlist {
  int id;
  int userId;
  String insuranceName;
  String packageName;
  String destination;
  int day;
  List<Prices> prices;
  int totalPrice;
  String createdAt;
  String updatedAt;

  TravelWishlist(
      {this.id,
      this.userId,
      this.insuranceName,
      this.packageName,
      this.destination,
      this.day,
      this.prices,
      this.totalPrice,
      this.createdAt,
      this.updatedAt});

  TravelWishlist.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    insuranceName = json['insurance_name'];
    packageName = json['package_name'];
    destination = json['destination'];
    day = json['day'];
    if (json['prices'] != null) {
      prices = new List<Prices>();
      json['prices'].forEach((v) {
        prices.add(new Prices.fromJson(v));
      });
    }
    totalPrice = json['total_price'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['insurance_name'] = this.insuranceName;
    data['package_name'] = this.packageName;
    data['destination'] = this.destination;
    data['day'] = this.day;
    if (this.prices != null) {
      data['prices'] = this.prices.map((v) => v.toJson()).toList();
    }
    data['total_price'] = this.totalPrice;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  static Future<List<TravelWishlist>> loadList(String token) async {
    http.Response response = await http.get(utils.url + '/api/wishlist/travel',
        headers: {"Authorization": "Bearer" + token});

    List collection = json.decode(response.body);
    List<TravelWishlist> _wishlists =
        collection.map((json) => TravelWishlist.fromJson(json)).toList();
    return _wishlists;
  }

  static Future<bool> delete(int wishlistId, String token) async {
    http.Response response = await http
        .delete(utils.url + '/api/wishlist/travel/$wishlistId', headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer" + token
    });
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }
}
