import 'package:easurance_mobile/models/Prices.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class PropertyWishlist {
  int id;
  int userId;
  String insuranceName;
  String province;
  String city;
  String address;
  int value;
  List<Prices> prices;
  int totalPrice;
  String createdAt;
  String updatedAt;

  PropertyWishlist(
      {this.id,
      this.userId,
      this.insuranceName,
      this.province,
      this.city,
      this.address,
      this.value,
      this.prices,
      this.totalPrice,
      this.createdAt,
      this.updatedAt});

  PropertyWishlist.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    insuranceName = json['insurance_name'];
    province = json['province'];
    city = json['city'];
    address = json['address'];
    value = json['value'] is int ? json['value'] : int.parse(json['value']);
    if (json['prices'] != null) {
      prices = new List<Prices>();
      json['prices'].forEach((v) {
        prices.add(new Prices.fromJson(v));
      });
    }
    totalPrice = json['total_price'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['insurance_name'] = this.insuranceName;
    data['province'] = this.province;
    data['city'] = this.city;
    data['address'] = this.address;
    data['value'] = this.value;
    if (this.prices != null) {
      data['prices'] = this.prices.map((v) => v.toJson()).toList();
    }
    data['total_price'] = this.totalPrice;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  static Future<List<PropertyWishlist>> loadList(String token) async {
    http.Response response = await http.get(
        utils.url + '/api/wishlist/property',
        headers: {"Authorization": "Bearer" + token});

    List collection = json.decode(response.body);
    List<PropertyWishlist> _wishlists =
        collection.map((json) => PropertyWishlist.fromJson(json)).toList();
    return _wishlists;
  }

  static Future<bool> delete(int wishlistId, String token) async {
    http.Response response = await http
        .delete(utils.url + '/api/wishlist/property/$wishlistId', headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer" + token
    });
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }
}
