import 'dart:convert';
import 'package:easurance_mobile/models/InsuranceBenefit.dart';
import 'package:easurance_mobile/models/Prices.dart';
import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class CarInsurancePremi {
  int insuranceId;
  String insuranceName;
  String insuranceLogoPath;
  List<InsuranceBenefits> insuranceBenefits;
  int insuranceWorkshopsCount;
  List<Prices> prices;
  int totalPrice;
  double matchValue = 0;

  CarInsurancePremi(
      {this.insuranceId,
      this.insuranceName,
      this.insuranceLogoPath,
      this.insuranceBenefits,
      this.insuranceWorkshopsCount,
      this.prices,
      this.totalPrice,
      this.matchValue});

  CarInsurancePremi.fromJson(Map<String, dynamic> json) {
    insuranceId = json['insurance_id'];
    insuranceName = json['insurance_name'];
    insuranceLogoPath = json['insurance_logo_path'];
    if (json['insurance_benefits'] != null) {
      insuranceBenefits = new List<InsuranceBenefits>();
      json['insurance_benefits'].forEach((v) {
        insuranceBenefits.add(new InsuranceBenefits.fromJson(v));
      });
    }
    insuranceWorkshopsCount = json['insurance_workshops_count'];
    if (json['prices'] != null) {
      prices = new List<Prices>();
      json['prices'].forEach((v) {
        prices.add(new Prices.fromJson(v));
      });
    }
    totalPrice = json['total_price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['insurance_id'] = this.insuranceId;
    data['insurance_name'] = this.insuranceName;
    data['insurance_logo_path'] = this.insuranceLogoPath;
    if (this.insuranceBenefits != null) {
      data['insurance_benefits'] =
          this.insuranceBenefits.map((v) => v.toJson()).toList();
    }
    data['insurance_workshops_count'] = this.insuranceWorkshopsCount;
    if (this.prices != null) {
      data['prices'] = this.prices.map((v) => v.toJson()).toList();
    }
    data['total_price'] = this.totalPrice;
    return data;
  }

  static Future<List<CarInsurancePremi>> loadList(
    String seriesId,
    String plateId,
    String type,
    String year,
    List<String> additional,
    int tplVal,
    int paDriverVal,
    int paPassengerVal,
    int paPassengerSeats,
  ) async {
    var url = utils.url + '/api/car/calculate-premium';
    Map data = {
      'car_series_id': seriesId,
      'car_plate_id': plateId,
      'insurance_type': type != null ? type : 'comp',
      'car_year': year,
      'additional': additional,
      'tpl_value': tplVal,
      'pa_driver_value': paDriverVal,
      'pa_passenger_value': paPassengerVal,
      'pa_passenger_seats': paPassengerSeats,
    };

    var body = json.encode(data);

    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    List collection = json.decode(response.body);
    List<CarInsurancePremi> _premis = sort(
        collection.map((json) => CarInsurancePremi.fromJson(json)).toList(),
        "name",
        true);

    return _premis;
  }

  static List<CarInsurancePremi> sort(
      List<CarInsurancePremi> premis, String sortBy, bool asc) {
    switch (sortBy) {
      case "name":
        asc
            ? premis.sort((a, b) => a.insuranceName.compareTo(b.insuranceName))
            : premis.sort((a, b) => b.insuranceName.compareTo(a.insuranceName));
        break;
      case "total":
        asc
            ? premis.sort((a, b) => a.totalPrice.compareTo(b.totalPrice))
            : premis.sort((a, b) => b.totalPrice.compareTo(a.totalPrice));
        break;
    }
    return premis;
  }

  static Future<List<CarInsurancePremi>> smartSystem(
      String seriesId,
      String plateId,
      String type,
      String year,
      List<String> additional,
      int tplVal,
      int paDriverVal,
      int paPassengerVal,
      int paPassengerSeats,
      List<InsuranceBenefits> selectedBenefit) async {
    var url = utils.url + '/api/car/calculate-premium';
    Map data = {
      'car_series_id': seriesId,
      'car_plate_id': plateId,
      'insurance_type': type,
      'car_year': year,
      'additional': additional,
      'tpl_value': tplVal,
      'pa_driver_value': paDriverVal,
      'pa_passenger_value': paPassengerVal,
      'pa_passenger_seats': paPassengerSeats,
    };

    var body = json.encode(data);

    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    List collection = json.decode(response.body);

    List<CarInsurancePremi> _premis =
        collection.map((json) => CarInsurancePremi.fromJson(json)).toList();

    for (CarInsurancePremi premi in _premis) {
      for (InsuranceBenefits benefit in selectedBenefit) {
        for (InsuranceBenefits benefit2 in premi.insuranceBenefits)
          if (benefit.id == benefit2.id) {
            premi.matchValue += 100 / selectedBenefit.length;
          }
      }
    }

    _premis.sort((a, b) => b.matchValue.compareTo(a.matchValue));

    return _premis.length < 3 ? _premis : _premis.sublist(0, 3);
  }

  static Future<bool> saveToWishlist(CarInsurancePremi premi, String carName,
      String carYear, String insuranceType, String token) async {
    var premium = premi.toJson();
    Map data = {
      'premium': premium,
      'car_name': carName,
      'car_year': carYear,
      'insurance_type': insuranceType,
    };
    var body = json.encode(data);
    http.Response response =
        await http.post(utils.url + '/api/wishlist/car', body: body, headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer" + token
    });
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }
}
