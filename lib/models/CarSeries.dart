class CarSeries {
  int id;
  int carTypeId;
  String name;
  String createdAt;
  String updatedAt;

  CarSeries(
      {this.id, this.carTypeId, this.name, this.createdAt, this.updatedAt});

  CarSeries.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    carTypeId = json['car_type_id'];
    name = json['name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['car_type_id'] = this.carTypeId;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
