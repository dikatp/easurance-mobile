import 'package:easurance_mobile/models/Prices.dart';
import 'dart:convert';
import 'package:easurance_mobile/utils.dart' as utils;

import 'package:http/http.dart' as http;

class CarWishlist {
  int id;
  int userId;
  String insuranceName;
  String carName;
  String carYear;
  String insuranceType;
  List<Prices> prices;
  int totalPrice;

  CarWishlist({
    this.id,
    this.userId,
    this.insuranceName,
    this.carName,
    this.carYear,
    this.insuranceType,
    this.prices,
    this.totalPrice,
  });

  CarWishlist.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    insuranceName = json['insurance_name'];
    carName = json['car_name'];
    carYear = json['car_year'];
    insuranceType = json['insurance_type'];
    if (json['prices'] != null) {
      prices = new List<Prices>();
      json['prices'].forEach((v) {
        prices.add(new Prices.fromJson(v));
      });
    }
    totalPrice = json['total_price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['insurance_name'] = this.insuranceName;
    data['car_name'] = this.carName;
    data['car_year'] = this.carYear;
    data['insurance_type'] = this.insuranceType;
    if (this.prices != null) {
      data['prices'] = this.prices.map((v) => v.toJson()).toList();
    }
    data['total_price'] = this.totalPrice;
    return data;
  }

  static Future<List<CarWishlist>> loadList(String token) async {
    http.Response response = await http.get(utils.url + '/api/wishlist/car',
        headers: {"Authorization": "Bearer" + token});

    List collection = json.decode(response.body);
    List<CarWishlist> _wishlists =
        collection.map((json) => CarWishlist.fromJson(json)).toList();
    return _wishlists;
  }

  static Future<bool> delete(int wishlistId, String token) async {
    http.Response response = await http
        .delete(utils.url + '/api/wishlist/car/$wishlistId', headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer" + token
    });
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }
}
