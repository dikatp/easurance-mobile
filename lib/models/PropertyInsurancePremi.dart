import 'dart:convert';
import 'package:easurance_mobile/models/InsuranceBenefit.dart';
import 'package:easurance_mobile/models/Prices.dart';
import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class PropertyInsurancePremi {
  int insuranceId;
  String insuranceName;
  String insuranceLogoPath;
  List<Prices> prices;
  int totalPrice;
  List<InsuranceBenefits> insuranceBenefits;
  double matchValue = 0;

  PropertyInsurancePremi(
      {this.insuranceId,
      this.insuranceName,
      this.insuranceLogoPath,
      this.prices,
      this.totalPrice,
      this.insuranceBenefits,
      this.matchValue});

  PropertyInsurancePremi.fromJson(Map<String, dynamic> json) {
    insuranceId = json['insurance_id'];
    insuranceName = json['insurance_name'];
    insuranceLogoPath = json['insurance_logo_path'];
    if (json['prices'] != null) {
      prices = new List<Prices>();
      json['prices'].forEach((v) {
        prices.add(new Prices.fromJson(v));
      });
    }
    totalPrice = json['total_price'];
    if (json['insurance_benefits'] != null) {
      insuranceBenefits = new List<InsuranceBenefits>();
      json['insurance_benefits'].forEach((v) {
        insuranceBenefits.add(new InsuranceBenefits.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['insurance_id'] = this.insuranceId;
    data['insurance_name'] = this.insuranceName;
    data['insurance_logo_path'] = this.insuranceLogoPath;
    if (this.prices != null) {
      data['prices'] = this.prices.map((v) => v.toJson()).toList();
    }
    data['total_price'] = this.totalPrice;
    if (this.insuranceBenefits != null) {
      data['insurance_benefits'] =
          this.insuranceBenefits.map((v) => v.toJson()).toList();
    }
    return data;
  }

  static Future<List<PropertyInsurancePremi>> loadList(
    int value,
    String selectedFlexas,
    String selectedCity,
    List<String> additional,
  ) async {
    var url = utils.url + '/api/property/calculate-premium';
    Map data = {
      'value': value,
      'class': selectedFlexas,
      'city': selectedCity,
      'additional': additional,
    };

    var body = json.encode(data);

    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    List collection = json.decode(response.body);
    List<PropertyInsurancePremi> _premis = sort(
        collection
            .map((json) => PropertyInsurancePremi.fromJson(json))
            .toList(),
        "name",
        true);

    return _premis;
  }

  static List<PropertyInsurancePremi> sort(
      List<PropertyInsurancePremi> premis, String sortBy, bool asc) {
    switch (sortBy) {
      case "name":
        asc
            ? premis.sort((a, b) => a.insuranceName.compareTo(b.insuranceName))
            : premis.sort((a, b) => b.insuranceName.compareTo(a.insuranceName));
        break;
      case "total":
        asc
            ? premis.sort((a, b) => a.totalPrice.compareTo(b.totalPrice))
            : premis.sort((a, b) => b.totalPrice.compareTo(a.totalPrice));
        break;
    }
    return premis;
  }

  static Future<List<PropertyInsurancePremi>> smartSystem(
      int value,
      String selectedFlexas,
      String selectedCity,
      List<String> additional,
      List<InsuranceBenefits> selectedBenefit) async {
    var url = utils.url + '/api/property/calculate-premium';
    Map data = {
      'value': value,
      'class': selectedFlexas,
      'city': selectedCity,
      'additional': additional,
    };

    var body = json.encode(data);

    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    List collection = json.decode(response.body);

    List<PropertyInsurancePremi> _premis = collection
        .map((json) => PropertyInsurancePremi.fromJson(json))
        .toList();

    for (PropertyInsurancePremi premi in _premis) {
      for (InsuranceBenefits benefit in selectedBenefit) {
        for (InsuranceBenefits benefit2 in premi.insuranceBenefits)
          if (benefit.id == benefit2.id) {
            premi.matchValue += 100 / selectedBenefit.length;
          }
      }
    }

    _premis.sort((a, b) => b.matchValue.compareTo(a.matchValue));

    return _premis.length < 3 ? _premis : _premis.sublist(0, 3);
  }

  static Future<bool> saveToWishlist(
      PropertyInsurancePremi premi,
      String province,
      String city,
      String address,
      int value,
      String token) async {
    var premium = premi.toJson();
    Map data = {
      'premium': premium,
      'province': province,
      'city': city,
      'address': address,
      'value': value,
    };

    var body = json.encode(data);
    http.Response response = await http
        .post(utils.url + '/api/wishlist/property', body: body, headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer" + token
    });
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }
}
