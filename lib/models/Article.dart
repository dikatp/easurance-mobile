import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class Article {
  int id;
  String title;
  String thumbnail;
  String content;
  String createdAt;
  String updatedAt;

  Article(
      {this.id,
      this.title,
      this.thumbnail,
      this.content,
      this.createdAt,
      this.updatedAt});

  Article.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    thumbnail = json['thumbnail'];
    content = json['content'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['thumbnail'] = this.thumbnail;
    data['content'] = this.content;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  static Future<List<Article>> loadList() async {
    http.Response response = await http.get(utils.url + '/api/articles');
    List collection = json.decode(response.body);
    List<Article> _articles =
        collection.map((json) => Article.fromJson(json)).toList();

    return _articles;
  }

  static Future<List<Article>> random() async {
    http.Response response = await http.get(utils.url + '/api/articles/random');
    List collection = json.decode(response.body);
    List<Article> _articles =
        collection.map((json) => Article.fromJson(json)).toList();

    return _articles;
  }
}
