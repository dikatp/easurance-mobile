class CarType {
  int id;
  int carBrandId;
  String name;
  String createdAt;
  String updatedAt;

  CarType(
      {this.id, this.carBrandId, this.name, this.createdAt, this.updatedAt});

  CarType.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    carBrandId = json['car_brand_id'];
    name = json['name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['car_brand_id'] = this.carBrandId;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
