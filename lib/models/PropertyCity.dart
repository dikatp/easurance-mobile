class PropertyCity {
  int id;
  int propertyProvinceId;
  String name;
  int zoneCode;
  String createdAt;
  String updatedAt;

  PropertyCity(
      {this.id,
      this.propertyProvinceId,
      this.name,
      this.zoneCode,
      this.createdAt,
      this.updatedAt});

  PropertyCity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    propertyProvinceId = json['property_province_id'];
    name = json['name'];
    zoneCode = json['zone_code'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['property_province_id'] = this.propertyProvinceId;
    data['name'] = this.name;
    data['zone_code'] = this.zoneCode;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
