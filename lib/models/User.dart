import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class User {
  int id;
  String name;
  String email;

  User({
    this.id,
    this.name,
    this.email,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    return data;
  }

  static Future<User> load(String token) async {
    http.Response response = await http.post(
      utils.url + '/api/auth/me',
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer" + token
      },
    );
    var data = json.decode(response.body);
    User user = User.fromJson(data);
    return user;
  }
}
