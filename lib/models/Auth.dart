import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:easurance_mobile/utils.dart' as utils;

class Auth {
  static final Auth _singleton =
      new Auth._internal(); //init auth class as singleton
  String _token; //auth api token
  bool _isLoaded =
      false; //for checking if the first load when the class is init are complete

  factory Auth() {
    //on new Auth() return singleton instance
    return _singleton;
  }

  Auth._internal() {
    //the constructor of class
    _load(); //try to init token
  }

  Future _load() async {
    final prefs =
        await SharedPreferences.getInstance(); //get sharedpref instance
    _token = prefs.getString('token') ??
        null; //try to get saved token from device or null if not found
    _isLoaded = true;
  }

  String get token {
    return _token;
  }

  Future<bool> check() async {
    if (!_isLoaded) //check if the first load is complete
      await _load(); //dunno how to wait till loaded.. try await load again
    if (_token != null) return true; // return true if token is not null
    return false;
  }

  Future<bool> login(String email, String password) async {
    http.Response response = await http.post(
      utils.url + '/api/auth/login',
      body: {
        'email': email,
        'password': password,
      },
      headers: {"Accept": "application/json"},
    );

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      _token = data['access_token'];
      return true;
    }
    return false;
  }

  void logout() {
    _token = null;
  }

  Future<bool> register(
      String email, String password, String password2, String name) async {
    http.Response response =
        await http.post(utils.url + '/api/auth/register', body: {
      'email': email,
      'password': password,
      'password_confirmation': password2,
      'name': name
    }, headers: {
      "Accept": "application/json"
    });
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }
}
