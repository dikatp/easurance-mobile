import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class Faq {
  int id;
  String question;
  String answer;
  String createdAt;
  String updatedAt;

  Faq({this.id, this.question, this.answer, this.createdAt, this.updatedAt});

  Faq.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    question = json['question'];
    answer = json['answer'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question'] = this.question;
    data['answer'] = this.answer;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  static Future<List<Faq>> loadList() async {
    http.Response response = await http.get(utils.url + '/api/faqs');
    List collection = json.decode(response.body);
    List<Faq> _faqs = collection.map((json) => Faq.fromJson(json)).toList();

    return _faqs;
  }
}
