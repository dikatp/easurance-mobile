import 'dart:convert';
import 'package:easurance_mobile/models/InsuranceBenefit.dart';
import 'package:easurance_mobile/models/Prices.dart';
import 'package:easurance_mobile/models/TravelDestination.dart';
import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class TravelInsurancePackage {
  String insuranceName;
  String insuranceLogoPath;
  List<InsuranceBenefits> insuranceBenefits;
  String package;
  List<Prices> prices;
  int totalPrice;
  int rateId;
  double matchValue = 0;

  TravelInsurancePackage(
      {this.insuranceName,
      this.insuranceLogoPath,
      this.insuranceBenefits,
      this.package,
      this.prices,
      this.totalPrice,
      this.rateId,
      this.matchValue});

  TravelInsurancePackage.fromJson(Map<String, dynamic> json) {
    insuranceName = json['insurance_name'];
    insuranceLogoPath = json['insurance_logo_path'];
    if (json['insurance_benefits'] != null) {
      insuranceBenefits = new List<InsuranceBenefits>();
      json['insurance_benefits'].forEach((v) {
        insuranceBenefits.add(new InsuranceBenefits.fromJson(v));
      });
    }
    package = json['package'];
    if (json['prices'] != null) {
      prices = new List<Prices>();
      json['prices'].forEach((v) {
        prices.add(new Prices.fromJson(v));
      });
    }
    totalPrice = json['total_price'];
    rateId = json['rate_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['insurance_name'] = this.insuranceName;
    data['insurance_logo_path'] = this.insuranceLogoPath;
    if (this.insuranceBenefits != null) {
      data['insurance_benefits'] =
          this.insuranceBenefits.map((v) => v.toJson()).toList();
    }
    data['package'] = this.package;
    if (this.prices != null) {
      data['prices'] = this.prices.map((v) => v.toJson()).toList();
    }
    data['total_price'] = this.totalPrice;
    data['rate_id'] = this.rateId;
    return data;
  }

  static Future<List<TravelInsurancePackage>> loadList(
    TravelDestination destinationId,
    String packageType,
    int duration,
  ) async {
    var url = utils.url + '/api/travel/calculate-premium';
    Map data = {
      'destination_id': destinationId.id,
      'package_type': packageType,
      'duration': duration,
    };

    var body = json.encode(data);

    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    List collection = json.decode(response.body);
    List<TravelInsurancePackage> _premis = sort(
        collection
            .map((json) => TravelInsurancePackage.fromJson(json))
            .toList(),
        "name",
        true);

    return _premis;
  }

  static List<TravelInsurancePackage> sort(
      List<TravelInsurancePackage> premis, String sortBy, bool asc) {
    switch (sortBy) {
      case "name":
        asc
            ? premis.sort((a, b) => a.insuranceName.compareTo(b.insuranceName))
            : premis.sort((a, b) => b.insuranceName.compareTo(a.insuranceName));
        break;
      case "total":
        asc
            ? premis.sort((a, b) => a.totalPrice.compareTo(b.totalPrice))
            : premis.sort((a, b) => b.totalPrice.compareTo(a.totalPrice));
        break;
    }
    return premis;
  }

  static Future<List<TravelInsurancePackage>> smartSystem(
      TravelDestination destinationId,
      String packageType,
      int duration,
      List<InsuranceBenefits> selectedBenefit) async {
    var url = utils.url + '/api/travel/calculate-premium';
    Map data = {
      'destination_id': destinationId.id,
      'package_type': packageType,
      'duration': duration,
    };

    var body = json.encode(data);

    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    List collection = json.decode(response.body);

    List<TravelInsurancePackage> _premis = collection
        .map((json) => TravelInsurancePackage.fromJson(json))
        .toList();

    for (TravelInsurancePackage premi in _premis) {
      for (InsuranceBenefits benefit in selectedBenefit) {
        for (InsuranceBenefits benefit2 in premi.insuranceBenefits)
          if (benefit.id == benefit2.id) {
            premi.matchValue += 100 / selectedBenefit.length;
          }
      }
    }

    _premis.sort((a, b) => b.matchValue.compareTo(a.matchValue));

    return _premis;
  }

  static Future<bool> saveToWishlist(TravelInsurancePackage package,
      TravelDestination destination, int duration, String token) async {
    var premium = package.toJson();
    Map data = {
      'premium': premium,
      'destination': destination.name,
      'day': duration
    };
    var body = json.encode(data);
    http.Response response = await http
        .post(utils.url + '/api/wishlist/travel', body: body, headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer" + token
    });
    if (response.statusCode == 200) {
      return true;
    }
    return false;
  }
}
