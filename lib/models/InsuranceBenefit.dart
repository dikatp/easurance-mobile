import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:easurance_mobile/utils.dart' as utils;

class InsuranceBenefits {
  int id;
  String name;
  String createdAt;
  String updatedAt;

  InsuranceBenefits({this.id, this.name, this.createdAt, this.updatedAt});

  InsuranceBenefits.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  static Future<List<InsuranceBenefits>> loadBenefits(
      String benefitType) async {
    http.Response response =
        await http.get(utils.url + '/api/$benefitType/benefits');
    List collection = json.decode(response.body);
    List<InsuranceBenefits> _benefits =
        collection.map((json) => InsuranceBenefits.fromJson(json)).toList();

    return _benefits;
  }
}
