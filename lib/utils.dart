import 'package:flutter/material.dart';

double baseHeight = 640.0;

double screenAwareSize(double size, BuildContext context) {
  return size * MediaQuery.of(context).size.height / baseHeight;
}

final primaryColor = Color(0xAA1dbb26);
final secondaryColor = Color(0xAAffc428);
final backgroundColor = Color(0xAAeaebea);

String url = 'http://192.168.1.110';
